<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::match(['get', 'post'], '/insert', 'HomeController@insert');
Route::match(['get', 'post'], '/kategori', 'HomeController@kategori');
Route::match(['get', 'post'], '/update/{id}', 'HomeController@update');
Route::match(['get', 'post'], '/delete/{id}', 'HomeController@delete');