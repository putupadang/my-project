@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-error alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{($error)}}</strong>    
                        </div>
                    @endforeach
                @endif
                <div class="card-header">Tambah Produk</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form action="{{url('kategori')}}" method="post" novalidate="novalidate">
                        {{ csrf_field() }}

                        <div class="control-group">
                            <label class="control-label">Nama Kategori</label>
                            <div class="controls">
                                <input type="text" name="nm_kategori" id="nm_kategori">
                            </div>
                        </div><br>

                        <div class="form-actions">
                            <input type="submit" value="Tambah" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
