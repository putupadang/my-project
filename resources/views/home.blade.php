@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-condensed data-table">
                        <thead>
                            <tr class="text-center">
                                <th>Nama Pemesan</th>
                                <th>Kategori Produk</th>
                                <th>Nama Produk</th>
                                <th>Harga Produk</th>
                                <th>Pilihan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $order)
                            <tr class="text-capitalize text-center">
                                <td>{{$user->name}}</td>
                                <td>{{$order->kategori}}</td>
                                <td>{{$order->nm_produk}}</td>
                                <td>{{$order->harga}}</td>
                                <td>
                                    <a href=" {{url('update/'.$order->id)}} " class="btn btn-primary btn-mini">Ubah</a>
                                    <a href=" {{url('delete/'.$order->id)}} " class="btn btn-danger btn-mini">Hapus</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
