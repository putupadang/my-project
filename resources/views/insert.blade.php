@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                @if (count($errors) > 0)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-error alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>
                            <strong>{{($error)}}</strong>    
                        </div>
                    @endforeach
                @endif
                <div class="card-header">Tambah Produk</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <form action="{{url('insert')}}" method="post" novalidate="novalidate">
                        {{ csrf_field() }}
                        
                        <input type="hidden" name="user_id" id="user_id" value="{{$user->id}}">

                        <div class="control-group">
                            <label class="control-label">Kategori Produk</label>
                            <div class="controls">
                                <select name="kategori_id" id="kategori_id">
                                    <option value="" selected disabled hidden>Pilih Kategori Produk</option>
                                    @foreach ($kategoris as $kategori)
                                        <option value="{{$kategori->id}}">{{$kategori->nm_kategori}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div><br>

                        <div class="control-group">
                            <label class="control-label">Nama Produk</label>
                            <div class="controls">
                                <input type="text" name="nm_produk" id="nm_produk">
                            </div>
                        </div><br>

                        <div class="control-group">
                            <label class="control-label">Harga Produk</label>
                            <div class="controls">
                                <input type="number" name="harga" id="harga">
                            </div>
                        </div><br>

                        <div class="form-actions">
                            <input type="submit" value="Tambah" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
