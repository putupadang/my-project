<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use App\User;
use App\Order;
use App\Kategori;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        // $order = User::find($user->id)->orders()->where('');
        // dd($order);
        $orders = Order::where([
            ['user_id', $user->id]
        ])->get();
        foreach ($orders as $key => $value) {
            $kategori = Kategori::where('id', $value->kategori_id)->first();
            // dd($kategori);
            $orders[$key]->kategori = $kategori->nm_kategori;
        }
        return view('home')->with(compact('user', 'orders'));
    }

    public function insert(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'user_id' => 'required',
                'kategori_id' => 'required',
                'nm_produk' => 'required',
                'harga' => 'required'
            ]);
            // $data = $request->all();
            // dd($data);

            $order = new Order();
            $order->user_id = $request->user_id;
            $order->kategori_id = $request->kategori_id;
            $order->nm_produk = $request->nm_produk;
            $order->harga = $request->harga;
            $order->save();

            return redirect('/home');
        }
        $user = Auth::user();
        $kategoris = Kategori::get();
        return view('insert')->with(compact('user', 'kategoris'));
    }

    public function update(Request $request, $id = null)
    {
        if ($request->isMethod('post')) {
            // $data = $request->all();
            // dd($data);
            $request->validate([
                'kategori_id' => 'required',
                'nm_produk' => 'required',
                'harga' => 'required'
            ]);

            Order::where('id', $id)->update([
                'kategori_id' => $request->kategori_id,
                'nm_produk' => $request->nm_produk,
                'harga' => $request->harga
            ]);

            return redirect('/home');
        }
        $kategoris = Kategori::get();
        $order = Order::where('id', $id)->first();
        return view('update')->with(compact('order', 'kategoris'));
    }

    public function delete(Request $request, $id = null)
    {
        Order::where('id', $id)->delete();
        return redirect()->back();
    }

    public function kategori(Request $request)
    {
        if ($request->isMethod('post')) {
            $request->validate([
                'nm_kategori' => 'required|unique:kategoris'
            ]);
            // $data = $request->all();
            // dd($data);

            $kategori = new Kategori();
            $kategori->nm_kategori = $request->nm_kategori;
            $kategori->save();

            return redirect('/insert');
        }
        return view('kategori');
    }
}

